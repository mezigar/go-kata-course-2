package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	fmt.Println(users)
	fmt.Println("users younger than 40 years")
	// New version
	i := 0
	for i < len(users)-1 {
		if users[i].Age > 40 {
			users = append(users[:i], users[i+1:]...)
			i--
		}
		i++
	}
	if users[i].Age > 40 {
		users = users[:i]
	}
	// Old Version
	//for i, user := range users {
	//	if user.Age > 40 {
	//		if i < len(users)-1 {
	//			copy(users[i:], users[i+1:])
	//		}
	//		users[len(users)-1] = User{Name: "", Age: 0}
	//		users = users[:len(users)-1]
	//	}
	fmt.Println(users)
	fmt.Println("removing first user")
	_, users = users[0], users[1:]
	fmt.Println(users)
	fmt.Println("removing last user")
	users, _ = users[:len(users)-1], users[len(users)-1]
	fmt.Println(users)
}

package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Address string
	City    string
	index   string
}

func main() {
	user := User{
		Age:  18,
		Name: "Alex",
	}
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	user.Wallet = wallet
	fmt.Println(user)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Address",
			City:    "Moscow",
			index:   "654321",
		},
	}
	fmt.Println(user2)
	fmt.Println("user allocates:", unsafe.Sizeof(user), "bytes")
}

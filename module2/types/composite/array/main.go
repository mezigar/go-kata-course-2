package main

import "fmt"

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	testArray()
	rangeArray()
}

func rangeArray() {
	users := [...]User{
		{
			Age:  15,
			Name: "Johan",
			Wallet: Wallet{
				RUR: 222000,
				USD: 2300,
				BTC: 5,
				ETH: 11,
			},
		},
		{
			Age:  21,
			Name: "Doe",
			Wallet: Wallet{
				RUR: 444000,
				USD: 3000,
				BTC: 1,
				ETH: 2,
			},
		},
	}
	fmt.Println("Users older than 18 years:")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}
	fmt.Println("Users with crypto in wallets:")
	for _, user := range users {
		if user.Wallet.BTC > 0 || user.Wallet.ETH > 0 {
			fmt.Println(user)
		}
	}
}

func testArray() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("original value:", a)
	a[0] = 21
	fmt.Println("changed first elem:", a)
	b := a
	a[0] = 233
	fmt.Println("original array:", a)
	fmt.Println("modified copy", b)
}

package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27_600,
		},
		{
			Name:  "https://github.com/kubernetes/kubernetes",
			Stars: 95_700,
		},
		{
			Name:  "https://github.com/apache/spark",
			Stars: 34_900,
		},
		{
			Name:  "https://github.com/Microsoft/vscode",
			Stars: 142_000,
		},
		{
			Name:  "https://github.com/NixOS/nixpkgs?ref=hackernoon.com",
			Stars: 11_400,
		},
		{
			Name:  "https://github.com/rust-lang/rust?ref=hackernoon.com",
			Stars: 77_500,
		},
		{
			Name:  "https://github.com/firehol/blocklist-ipsets?ref=hackernoon.com",
			Stars: 2_500,
		},
		{
			Name:  "https://github.com/openshift/origin?ref=hackernoon.com",
			Stars: 8_300,
		},
		{
			Name:  "https://github.com/ansible/ansible?ref=hackernoon.com",
			Stars: 56_200,
		},
		{
			Name:  "https://github.com/Automattic/wp-calypso?ref=hackernoon.com",
			Stars: 12_200,
		},
		{
			Name:  "https://github.com/dotnet/corefx?ref=hackernoon.com",
			Stars: 17_900,
		},
		{
			Name:  "https://github.com/nodejs/node?ref=hackernoon.com",
			Stars: 93_200,
		},
		// сюда впишите ваши остальные 12 структур
	}

	// в цикле запишите в map
	NameToStars := make(map[string]Project)
	for i := range projects {
		NameToStars[projects[i].Name] = projects[i]
	}
	for key, elem := range NameToStars {
		fmt.Printf("Project %s with %v stars\n", key, elem)
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
}

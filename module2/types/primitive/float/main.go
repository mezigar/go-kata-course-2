package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== START type float ===")
	// расчет экспоненты
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	// добавление мантиссу
	uintNumber += 1 << 21
	// скеякм знак на минус
	uintNumber += 1 << 31
	var floatNumber float32 = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)
	fmt.Println("=== END type float ===")
}

package main

import "fmt"

func main() {
	var numberInt int = 3
	var numberFloat float32 = float32(numberInt)
	fmt.Printf("type %T, value %v\n", numberFloat, numberFloat)

	var numberByte byte = 113
	// not working
	//var numberComplex complex64 = complex64(numberByte)
	//misscast
	//var numberFloat2 float64 = float32(numberByte)
	var numberFloat3 float64 = float64(numberByte)
	fmt.Printf("type %T, value %v\n", numberFloat3, numberFloat3)
}

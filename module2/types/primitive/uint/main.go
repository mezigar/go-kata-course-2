package main

import (
	"fmt"
	"unsafe"
)

func main() {
	numberRightBitwise := 1024 >> 4
	fmt.Println("right shift uint8:", numberRightBitwise)
	typeUint()
	typeByte()
	typeBool()
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint16 max value:", numberUint32, unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint16 max value:", numberUint64, unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint")
}

func typeByte() {
	var b byte = 124
	fmt.Println("size of byte in bytes:", unsafe.Sizeof(b))
}

func typeBool() {
	var b bool
	fmt.Println("size of bool in bytes:", unsafe.Sizeof(b))

}

package main

import "testing"

var jsonData = `
	{
	  "id": 0,
	  "category": {
		"id": 0,
		"name": "string"
	  },
	  "name": "doggie",
	  "photoUrls": [
		"string"
	  ],
	  "tags": [
		{
		  "id": 0,
		  "name": "string"
		}
	  ],
	  "status": "available"
	}
	`

func BenchmarkUnmarshalSTL(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalSTL([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkUnmarshalJsoniter(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalJsoniter([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}

}

var pet, _ = UnmarshalJsoniter([]byte(jsonData))

func BenchmarkMarshalSTL(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err := pet.MarshalSTL()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkMarshalJsoniter(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err := pet.MarshalJsoniter()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

func UnmarshalSTL(data []byte) (Pet, error) {
	var p Pet
	err := json.Unmarshal(data, &p)
	return p, err
}

func UnmarshalJsoniter(data []byte) (Pet, error) {
	var p Pet
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	err := json.Unmarshal(data, &p)
	return p, err
}

func (p *Pet) MarshalSTL() ([]byte, error) {
	return json.Marshal(p)
}

func (p *Pet) MarshalJsoniter() ([]byte, error) {
	return jsoniter.Marshal(p)
}

type Pet struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

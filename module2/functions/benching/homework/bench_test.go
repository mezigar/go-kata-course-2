package main

import (
	"testing"
)

var users []User = genUsers()
var products []Product = genProducts()

func BenchmarkMapUserProducts(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}

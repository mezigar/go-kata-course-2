package benching

import (
	"testing"
)

//func BenchmarkSimplestNTimes(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		// Здесь выполняем функцию для тестирования
//	}
//}

// insertXPreallocIntMap - для добавления X элементов в Map[int]int
func insertXPreallocIntMap(x int, b *testing.B) {
	// Инициализируем Map, вставляем X элементов и заранее предустанавливаем размер X
	testmap := make(map[int]int, x)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}

// BenchmarkInsertIntMap100000 тестирует скорость вставки 100000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100000, b)
	}
}

// BenchmarkInsertIntMap10000 тестирует скорость вставки 10000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(10000, b)
	}
}

// BenchmarkInsertIntMap1000 тестирует скорость вставки 1000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(1000, b)
	}
}

// BenchmarkInsertIntMap100 тестирует скорость вставки 100 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100, b)
	}
}

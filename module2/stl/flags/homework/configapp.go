package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
)

type Config struct {
	AppName    string
	Production bool
}

func NewConfigCommand() *ConfigCommand {
	cc := &ConfigCommand{
		fs: flag.NewFlagSet("-conf", flag.ContinueOnError),
	}
	cc.fs.StringVar(&cc.filepath, "conf", "./config.json", "path to the config.json")
	return cc
}

type ConfigCommand struct {
	fs *flag.FlagSet

	filepath string
}

func (c *ConfigCommand) Name() string {
	return c.fs.Name()
}

func (c *ConfigCommand) Init(args []string) error {
	return c.fs.Parse(args)
}

func (c *ConfigCommand) Run() error {
	jsonFile, err := os.Open(c.filepath)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer jsonFile.Close()

	var config Config

	byteValue, _ := io.ReadAll(jsonFile)
	err1 := json.Unmarshal(byteValue, &config)
	if err1 != nil {
		fmt.Println("Unmarshal Error:", err)
		os.Exit(1)
	}

	fmt.Println(config.AppName)
	fmt.Println(config.Production)

	return nil
}

type Runner interface {
	Init([]string) error
	Run() error
	Name() string
}

func root(args []string) error {
	if len(args) < 1 {
		return errors.New("You must pass a sub-command!")
	}
	cmds := []Runner{
		NewConfigCommand(),
	}
	// нужно изменить строчку ниже? Не очень понял, как вытащить необходимый мне аргумент
	subcommand := args[0]
	var filepath string
	flag.StringVar(&filepath, "conf", "./config.json", "path to the config.json")
	flag.Parse()
	args = []string{filepath}
	for _, cmd := range cmds {
		if cmd.Name() == subcommand {
			err := cmd.Init(args)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			return cmd.Run()
		}
	}
	return fmt.Errorf("Unknown subcommand: %s", subcommand)
}

func main() {
	if err := root(os.Args[1:]); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение

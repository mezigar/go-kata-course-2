package main

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./gopher.db")

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname VARCHAR(50), lastname VAECHAR(50))")

	_, err := statement.Exec()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")

	_, err = statement.Exec("John", "Weak")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	rows, err := database.Query("SELECT id, firstname, lastname FROM people")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var id int
	var firstname, lastname string

	for rows.Next() {
		err = rows.Scan(&id, &firstname, &lastname)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Printf("%d: %s, %s\n", id, firstname, lastname)
	}
}

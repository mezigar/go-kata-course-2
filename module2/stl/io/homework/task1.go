// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet\n",
		"чистый код лучше, чем заумный код\n",
		"ваш код станет наследием будущих программистов\n",
		"задумайтесь об этом\n",
	}
	// здесь расположите буфер
	var writer bytes.Buffer
	// запишите данные в буфер
	for _, p := range data {
		n, err := writer.Write([]byte(p))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n != len(p) {
			fmt.Println("failed to write data")
			os.Exit(1)
		}
	}
	// создайте файл
	f, err := os.Create("./dataFile.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	buf := make([]byte, 1024)
	// запишите данные в файл
	for {
		n, err := writer.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println(err)
			os.Exit(1)
		}
		if n == 0 {
			break
		}
		n1, err1 := f.Write(buf)
		if err1 != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n1 != len(buf) {
			fmt.Println("failed to write data")
			os.Exit(1)
		}
	}
	err2 := f.Close()
	if err != nil {
		fmt.Println(err2)
		os.Exit(1)
	}

	f1, err1 := os.Open("./dataFile.txt")
	if err1 != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	//fmt.Println(buf)
	//reader := bufio.NewReader(f)
	reader := io.Reader(f1)
	// прочтите данные в новый буфе
	buf1 := make([]byte, 1024)
	for {
		n, err := reader.Read(buf1)
		if err != nil && err != io.EOF {
			fmt.Println(err)
			os.Exit(1)
		}
		if n == 0 {
			break
		}
	}
	fmt.Println(string(buf1)) // выведите данные из буфера buffer.String()
	// у вас все получится!
}

package main

import (
	"bufio"
	"os"
)

var baseRuEn = map[string]string{
	"а": "a", "А": "A", "Б": "B", "б": "b", "В": "V", "в": "v", "Г": "G", "г": "g",
	"Д": "D", "д": "d", "З": "Z", "з": "z", "И": "I", "и": "i", "К": "K", "к": "k",
	"Л": "L", "л": "l", "М": "M", "м": "m", "Н": "N", "н": "n", "О": "O", "о": "o",
	"П": "P", "п": "p", "Р": "R", "р": "r", "С": "S", "с": "s", "Т": "T", "т": "t",
	"У": "U", "у": "u", "Ф": "F", "ф": "f",
}

func translit(data string) string {
	var result string
	for _, ch := range data {
		if val, ok := baseRuEn[string(ch)]; ok {
			result += val
		} else {
			result += string(ch)
		}
	}
	return result
}

func main() {
	// инициализирую необходимые переменные, настраиваю соединение
	f1, _ := os.Open("example.txt")
	f2, _ := os.Create("example.processed.txt")
	w := bufio.NewWriter(f2)
	scanner := bufio.NewScanner(f1)
	// не забываем освободить ресурсы
	defer f1.Close()
	defer f2.Close()
	defer w.Flush()
	for scanner.Scan() {
		txt := scanner.Text()
		_, _ = w.WriteString(translit(txt))
	}

}

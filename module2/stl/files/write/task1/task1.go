package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)
	// Проблема, что файл создается в корне каталога, не нашёл как это пофиксить
	f, err := os.Create("task1.txt")
	check(err)
	w := bufio.NewWriter(f)
	for {
		fmt.Print("Type (0 is exit): ")
		data, _ := reader.ReadString('\n')
		if data == "0\n" {
			break
		} else {
			_, _ = w.WriteString(data)
		}
	}
	w.Flush()
	f.Close()
}

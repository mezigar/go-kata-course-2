package main

import (
	"fmt"
	"math/rand"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func (p Person) generateSelfStory() string {
	return fmt.Sprintf("Hello! My name is %s. I'm %d y.o. And I also have %2.2f in my wallet right now.", p.Name, p.Age, p.Money)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
func main() {
	personsInStories := 7
	lettersInNames := 8
	maxAge := 50
	maxMoney := 1_000_000
	persons := make([]Person, personsInStories)

	for i := 0; i < personsInStories; i++ {
		persons[i] = Person{Name: RandStringBytes(lettersInNames), Age: rand.Intn(maxAge), Money: rand.ExpFloat64() * float64(maxMoney)}
		fmt.Println(persons[i].generateSelfStory())
	}
}

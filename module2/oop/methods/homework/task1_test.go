package main

import (
	"testing"
)

type args struct {
	a, b float64
}

type test struct {
	name string
	args args
	want float64
}

func TestSum(t *testing.T) {
	tests := []test{
		{
			name: "simple test 1",
			args: args{
				a: 1,
				b: 2,
			},
			want: 3,
		},
		{
			name: "simple test 2",
			args: args{
				a: 2,
				b: 2,
			},
			want: 4,
		},
		{
			name: "simple test 3",
			args: args{
				a: 5,
				b: -1,
			},
			want: 4,
		},
		{
			name: "simple test 4",
			args: args{
				a: 0,
				b: 0,
			},
			want: 0,
		},
		{
			name: "smart test 1",
			args: args{
				a: 100,
				b: 2,
			},
			want: 102,
		},
		{
			name: "smart test 2",
			args: args{
				a: -1,
				b: -3,
			},
			want: -4,
		},
		{
			name: "smart test 3",
			args: args{
				a: -1,
				b: 1,
			},
			want: 0,
		},
		{
			name: "smart test 4",
			args: args{
				a: 111,
				b: 2,
			},
			want: 113,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Sum() = #{got}, want #{tt.want}")
			}
		})
	}
}

func TestDivide(t *testing.T) {
	tests := []test{
		{
			name: "simple test 1",
			args: args{
				a: 10,
				b: 2,
			},
			want: 5.0,
		},
		{
			name: "simple test 2",
			args: args{
				a: 2,
				b: 2,
			},
			want: 1.0,
		},
		{
			name: "simple test 3",
			args: args{
				a: 5,
				b: -1,
			},
			want: -5.0,
		},
		{
			name: "simple test 4",
			args: args{
				a: 0,
				b: 1,
			},
			want: 0.0,
		},
		{
			name: "smart test 1",
			args: args{
				a: 100,
				b: 2,
			},
			want: 50.0,
		},
		{
			name: "smart test 2",
			args: args{
				a: -15,
				b: -3,
			},
			want: 5.0,
		},
		{
			name: "smart test 3",
			args: args{
				a: -1,
				b: 1,
			},
			want: -1.0,
		},
		{
			name: "smart test 4",
			args: args{
				a: 111,
				b: 2,
			},
			want: 55.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Sum() = #{got}, want #{tt.want}")
			}
		})
	}
}

func TestAvarage(t *testing.T) {
	tests := []test{
		{
			name: "simple test 1",
			args: args{
				a: 10,
				b: 2,
			},
			want: 6.0,
		},
		{
			name: "simple test 2",
			args: args{
				a: 2,
				b: 2,
			},
			want: 2.0,
		},
		{
			name: "simple test 3",
			args: args{
				a: 5,
				b: -1,
			},
			want: 2.0,
		},
		{
			name: "simple test 4",
			args: args{
				a: 0,
				b: 1,
			},
			want: 0.5,
		},
		{
			name: "smart test 1",
			args: args{
				a: 100,
				b: 2,
			},
			want: 51.0,
		},
		{
			name: "smart test 2",
			args: args{
				a: -15,
				b: -3,
			},
			want: -9.0,
		},
		{
			name: "smart test 3",
			args: args{
				a: -1,
				b: 1,
			},
			want: 0.0,
		},
		{
			name: "smart test 4",
			args: args{
				a: 111,
				b: 2,
			},
			want: 56.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := avarage(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Sum() = #{got}, want #{tt.want}")
			}
		})
	}
}

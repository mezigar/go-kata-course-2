package repo

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

var basicOrder = models.Order{
	ID:       1,
	PetID:    1,
	Quantity: 1,
	ShipDate: "some date",
	Status:   "sold",
	Complete: true,
}

func TestOrderStorage_Create(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Order
		primaryKeyIDx      map[int]*models.Order
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		order models.Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Order
	}{
		{
			name: "Correct Create",
			fields: fields{
				filepath:           "./order_repo.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				basicOrder,
			},
			want: basicOrder,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := o.Create(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderStorage.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderStorage_Delete(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Order
		primaryKeyIDx      map[int]*models.Order
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		orderId int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Correct Delete",
			fields: fields{
				filepath:           "./order_repo_delete.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				orderId: 1,
			},
			wantErr: false,
		},
		{
			name: "Incorrect Delete",
			fields: fields{
				filepath:           "./order_repo_delete.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				orderId: 2,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			o.Create(basicOrder)
			if err := o.Delete(tt.args.orderId); (err != nil) != tt.wantErr {
				t.Errorf("OrderStorage.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderStorage_GetByID(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Order
		primaryKeyIDx      map[int]*models.Order
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		orderId int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Order
		wantErr bool
	}{
		{
			name: "Correct GetById",
			fields: fields{
				filepath:           "./order_repo_delete.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				orderId: 1,
			},
			want:    basicOrder,
			wantErr: false,
		},
		{
			name: "Incorrect GetById",
			fields: fields{
				filepath:           "./order_repo_delete.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				orderId: 2,
			},
			want:    models.Order{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			o.Create(basicOrder)
			got, err := o.GetByID(tt.args.orderId)
			if (err != nil) != tt.wantErr {
				t.Errorf("OrderStorage.GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderStorage.GetByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderStorage_GetList(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Order
		primaryKeyIDx      map[int]*models.Order
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []models.Order
	}{
		{
			name: "Correct GetById",
			fields: fields{
				filepath:           "./order_repo_delete.json",
				data:               make([]*models.Order, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Order, 5),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			want: []models.Order{basicOrder},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			o.Create(basicOrder)
			if got := o.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OrderStorage.GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

package repo

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

type OrderStorager interface {
	Create(order models.Order) models.Order
	Delete(orderId int) error
	GetByID(orderId int) (models.Order, error)
	GetList() []models.Order
}

type OrderStorage struct {
	filepath           string
	data               []*models.Order
	primaryKeyIDx      map[int]*models.Order
	autoIncrementCount int
	*sync.Mutex
}

func NewOrderStorage(filepath string) *OrderStorage {
	return &OrderStorage{
		filepath:           filepath,
		data:               make([]*models.Order, 0, 10),
		primaryKeyIDx:      make(map[int]*models.Order, 13),
		autoIncrementCount: 1,
	}
}

func (o *OrderStorage) saveOrders(orders []*models.Order) error {
	valOrders := fromPointersToValueOrders(orders)
	data, err := json.MarshalIndent(valOrders, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	f, err := os.OpenFile(o.filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	if err = f.Truncate(0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Write(data); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func fromPointersToValueOrders(orders []*models.Order) []models.Order {
	valuesOrders := make([]models.Order, 0, len(orders))
	for i := range orders {
		valuesOrders = append(valuesOrders, *orders[i])
	}
	return valuesOrders
}

func (o *OrderStorage) Create(order models.Order) models.Order {
	o.Lock()
	defer o.Unlock()
	order.ID = o.autoIncrementCount
	o.primaryKeyIDx[order.ID] = &order
	o.autoIncrementCount++
	o.data = append(o.data, &order)
	if err := o.saveOrders(o.data); err != nil {
		fmt.Println(err)
	}

	return order
}

func (o *OrderStorage) Delete(orderId int) error {
	o.Lock()
	defer o.Unlock()
	for idx, deletingOrder := range o.data {
		if orderId == deletingOrder.ID {
			o.data = append(o.data[:idx], o.data[idx+1:]...)
			if err := o.saveOrders(o.data); err != nil {
				fmt.Println(err)
				return err
			}
			return nil
		}
	}
	return fmt.Errorf("No order with such ID")
}

func (o *OrderStorage) GetByID(orderId int) (models.Order, error) {
	o.Lock()
	defer o.Unlock()
	if v, ok := o.primaryKeyIDx[orderId]; ok {
		return *v, nil
	}

	return models.Order{}, fmt.Errorf("Order with such id not found")
}

func (o *OrderStorage) GetList() []models.Order {
	o.Lock()
	defer o.Unlock()
	data := make([]models.Order, 0, len(o.data))
	for i := range o.data {
		data = append(data, *o.data[i])
	}
	return data
}

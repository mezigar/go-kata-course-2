package repo

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

type UserStorager interface {
	Create(user models.User) models.User
	Update(user models.User) (models.User, error)
	Delete(username string) error
	GetByUsername(username string) (models.User, error)
	GetList() []models.User
}

type UserStorage struct {
	filepath           string
	data               []*models.User
	primaryKeyIDx      map[string]*models.User
	autoIncrementCount int
	*sync.Mutex
}

func NewUserStorage(filepath string) *UserStorage {
	return &UserStorage{
		filepath:           filepath,
		data:               make([]*models.User, 0, 10),
		primaryKeyIDx:      make(map[string]*models.User, 10),
		autoIncrementCount: 1,
	}
}

func (u *UserStorage) saveUsers(users []*models.User) error {
	valUsers := fromPointersToValueUsers(users)
	data, err := json.MarshalIndent(valUsers, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	f, err := os.OpenFile(u.filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	if err = f.Truncate(0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Write(data); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func fromPointersToValueUsers(users []*models.User) []models.User {
	valuesUsers := make([]models.User, 0, len(users))
	for i := range users {
		valuesUsers = append(valuesUsers, *users[i])
	}
	return valuesUsers
}

func (u *UserStorage) Create(user models.User) models.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.Username] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)
	if err := u.saveUsers(u.data); err != nil {
		fmt.Println(err)
	}

	return user
}

func (u *UserStorage) Delete(username string) error {
	u.Lock()
	defer u.Unlock()
	for idx, deletingUser := range u.data {
		if username == deletingUser.Username {
			u.data = append(u.data[:idx], u.data[idx+1:]...)
			if err := u.saveUsers(u.data); err != nil {
				fmt.Println(err)
				return err
			}
			return nil
		}
	}
	return fmt.Errorf("No user with such username")
}

func (u *UserStorage) GetByUsername(username string) (models.User, error) {
	u.Lock()
	defer u.Unlock()
	if v, ok := u.primaryKeyIDx[username]; ok {
		return *v, nil
	}

	return models.User{}, fmt.Errorf("User with such username not found")
}

func (u *UserStorage) GetList() []models.User {
	u.Lock()
	defer u.Unlock()
	data := make([]models.User, 0, len(u.data))
	for i := range u.data {
		data = append(data, *u.data[i])
	}
	return data
}

func (u *UserStorage) Update(user models.User) (models.User, error) {
	u.Lock()
	defer u.Unlock()
	for idx, updatingUser := range u.data {
		if user.Username == updatingUser.Username {
			u.data[idx] = &user
			if err := u.saveUsers(u.data); err != nil {
				fmt.Println(err)
				return models.User{}, err
			}
			return user, nil
		}
	}
	return user, fmt.Errorf("No user with such username")
}

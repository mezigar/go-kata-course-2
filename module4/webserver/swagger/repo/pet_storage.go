package repo

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

type PetStorager interface {
	Create(pet models.Pet) models.Pet
	Update(pet models.Pet) (models.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (models.Pet, error)
	GetList() []models.Pet
}

type PetStorage struct {
	filepath           string
	data               []*models.Pet
	primaryKeyIDx      map[int]*models.Pet
	autoIncrementCount int
	*sync.Mutex
}

func NewPetStorage(filepath string) *PetStorage {
	return &PetStorage{
		filepath:           filepath,
		data:               make([]*models.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*models.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) savePets(pets []*models.Pet) error {
	valPets := fromPointersToValuePets(pets)
	data, err := json.MarshalIndent(valPets, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	f, err := os.OpenFile(p.filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	if err = f.Truncate(0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Write(data); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func fromPointersToValuePets(pets []*models.Pet) []models.Pet {
	valuesPets := make([]models.Pet, 0, len(pets))
	for i := range pets {
		valuesPets = append(valuesPets, *pets[i])
	}
	return valuesPets
}

func (p *PetStorage) Create(pet models.Pet) models.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	if err := p.savePets(p.data); err != nil {
		fmt.Println(err)
	}
	return pet
}

func (p *PetStorage) Update(pet models.Pet) (models.Pet, error) {
	p.Lock()
	defer p.Unlock()
	for idx, updatingPet := range p.data {
		if updatingPet.ID == pet.ID {
			p.data[idx] = &pet
			if err := p.savePets(p.data); err != nil {
				fmt.Println(err)
				return models.Pet{}, err
			}
			return pet, nil
		}
	}
	return pet, fmt.Errorf("No pet with such id")
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()
	for idx, deletingPet := range p.data {
		if petID == deletingPet.ID {
			p.data = append(p.data[:idx], p.data[idx+1:]...)
			if err := p.savePets(p.data); err != nil {
				fmt.Println(err)
				return err
			}
			return nil
		}
	}
	return fmt.Errorf("No pet with such ID")
}

func (p *PetStorage) GetByID(petID int) (models.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}

	return models.Pet{}, fmt.Errorf("Pet with such id not found")
}

func (p *PetStorage) GetList() []models.Pet {
	p.Lock()
	defer p.Unlock()
	data := make([]models.Pet, 0, len(p.data))
	for i := range p.data {
		data = append(data, *p.data[i])
	}
	return data
}

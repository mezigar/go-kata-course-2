package repo

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

var basicPet = models.Pet{
	ID: 1,
	Category: models.Category{
		ID:   1,
		Name: "VIP",
	},
	Name:      "Zues",
	PhotoUrls: nil,
	Tags:      nil,
	Status:    "sold",
}

var basicPet2 = models.Pet{
	ID: 2,
	Category: models.Category{
		ID:   1,
		Name: "Healthy",
	},
	Name:      "Venera",
	PhotoUrls: nil,
	Tags:      nil,
	Status:    "sold",
}

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Pet
	}{
		{
			name: "Correct create",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				pet: basicPet,
			},
			want: basicPet,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := p.Create(tt.args.pet); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PetStorage.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Pet
		wantErr bool
	}{
		{
			name: "Correct update",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				pet: basicPet,
			},
			want:    basicPet,
			wantErr: false,
		},
		{
			name: "Incorrect update",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				pet: basicPet2,
			},
			want:    basicPet2,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			p.Create(basicPet)
			got, err := p.Update(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("PetStorage.Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PetStorage.Update() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Correct Delete",
			fields: fields{
				filepath:           "./pet_repo_delete.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				petID: 1,
			},
			wantErr: false,
		},
		{
			name: "Incorrect Delete",
			fields: fields{
				filepath:           "./pet_repo_delete.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				petID: 2,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			p.Create(basicPet)
			if err := p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("PetStorage.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Pet
		wantErr bool
	}{
		{
			name: "Correct getByID",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				petID: 1,
			},
			want:    basicPet,
			wantErr: false,
		},
		{
			name: "Incorrect getByID",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				petID: 2,
			},
			want:    models.Pet{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			p.Create(basicPet)
			got, err := p.GetByID(tt.args.petID)
			if (err != nil) != tt.wantErr {
				t.Errorf("PetStorage.GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PetStorage.GetByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetList(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []models.Pet
	}{
		{
			name: "Correct getList",
			fields: fields{
				filepath:           "./pet_repo.json",
				data:               make([]*models.Pet, 0, 10),
				primaryKeyIDx:      make(map[int]*models.Pet, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			want: []models.Pet{basicPet, basicPet2},
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			p.Create(basicPet)
			p.Create(basicPet2)
			if got := p.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PetStorage.GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

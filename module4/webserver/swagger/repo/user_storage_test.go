package repo

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

var basicUser = models.User{
	ID:         1,
	Username:   "mezigar",
	FirstName:  "Evgeniy",
	LastName:   "Muratov",
	Email:      "mail@mail.ru",
	Password:   "password",
	Phone:      "888888888",
	UserStatus: 1,
}

var basicUser2 = models.User{
	ID:         2,
	Username:   "hegizt",
	FirstName:  "Egor",
	LastName:   "Stepanov",
	Email:      "mail@mail.ru",
	Password:   "password",
	Phone:      "888888888",
	UserStatus: 1,
}

func TestUserStorage_Create(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.User
		primaryKeyIDx      map[string]*models.User
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.User
	}{
		{
			name: "Correct Create",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				basicUser,
			},
			want: basicUser,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := u.Create(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserStorage.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.User
		primaryKeyIDx      map[string]*models.User
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Correct Deleting",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				username: "mezigar",
			},
			wantErr: false,
		},
		{
			name: "Incorrect Deleting",
			fields: fields{
				filepath:           "./user_repo_delete.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				username: "Notmezigar",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			u.Create(basicUser)
			if err := u.Delete(tt.args.username); (err != nil) != tt.wantErr {
				t.Errorf("UserStorage.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByUsername(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.User
		primaryKeyIDx      map[string]*models.User
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name: "Correct Get",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				username: "mezigar",
			},
			want:    basicUser,
			wantErr: false,
		},
		{
			name: "Incorrect Get",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				username: "Notmezigar",
			},
			want:    models.User{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			u.Create(basicUser)
			got, err := u.GetByUsername(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserStorage.GetByUsername() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserStorage.GetByUsername() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_GetList(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.User
		primaryKeyIDx      map[string]*models.User
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []models.User
	}{
		{
			name: "Correct List",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			want: []models.User{basicUser, basicUser2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			u.Create(basicUser)
			u.Create(basicUser2)
			if got := u.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserStorage.GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	type fields struct {
		filepath           string
		data               []*models.User
		primaryKeyIDx      map[string]*models.User
		autoIncrementCount int
		Mutex              *sync.Mutex
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name: "Correct Update",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				user: basicUser,
			},
			want:    basicUser,
			wantErr: false,
		},
		{
			name: "Incorrect Update",
			fields: fields{
				filepath:           "./user_repo.json",
				data:               make([]*models.User, 0, 10),
				primaryKeyIDx:      make(map[string]*models.User, 10),
				autoIncrementCount: 1,
				Mutex:              &sync.Mutex{},
			},
			args: args{
				user: basicUser2,
			},
			want:    basicUser2,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				filepath:           tt.fields.filepath,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			u.Create(basicUser)
			got, err := u.Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserStorage.Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserStorage.Update() = %v, want %v", got, tt.want)
			}
		})
	}
}

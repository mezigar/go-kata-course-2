package main

import (
	"os"

	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
)

func init() {
	_ = petAddRequest{}
	_ = petAddResponse{}
	_ = petDeleteByIDRequest{}
	_ = petGetByIDRequest{}
	_ = petGetByIDResponse{}
	_ = petGetByStatusRequest{}
	_ = petGetByStatusResponse{}
	_ = petUpdateByIDRequest{}
	_ = petUpdateByIDResponse{}
	_ = petUpdateRequest{}
	_ = petUpdateResponse{}
	_ = petUploadImageRequest{}
	_ = petUploadImageResponse{}

	_ = deleteOrderRequest{}
	_ = deleteOrderResponse{}
	_ = getOrderRequest{}
	_ = getOrderResponse{}
	_ = inventoryOrderRequest{}
	_ = inventoryOrderResponse{}
	_ = postOrderRequest{}
	_ = postOrderResponse{}

	_ = userArrayCreateRequest{}
	_ = userCreateRequest{}
	_ = userDeleteRequest{}
	_ = userDeleteResponse{}
	_ = userGetRequest{}
	_ = userGetResponse{}
	_ = userPutRequest{}
	_ = userPutResponse{}
}

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:route PUT /pet pet petUpdateRequest
// Обновление существующего питомца.
// responses:
//	200: petUpdateResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID питомца
	// in:path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:route POST /pet/{id} pet petUpdateByIDRequest
// Обновление питомца  по id.
// responses:
//	200: petUpdateByIDResponse

// swagger:parameters petUpdateByIDRequest
type petUpdateByIDRequest struct {
	// ID питомца
	// required:true
	// in:path
	ID string `json:"id"`

	// Обновленное имя питомца
	// in:formData
	Name string `json:"name"`

	// Обновленный статус питомца
	// in:formData
	Status string `json:"status"`
}

// swagger:response petUpdateByIDResponse
type petUpdateByIDResponse struct {
	// in:body
	Body models.Pet `json:"body"`
}

// swagger:route DELETE /pet/{id} pet petDeleteByIDRequest
// Удаление питомца по id.
// responses:
//	 200: description: Deleted successfuly.

// swagger:parameters petDeleteByIDRequest
type petDeleteByIDRequest struct {
	// ID питомца
	// required:true
	// in:path
	ID string `json:"id"`
}

// swagger:route GET /pet/findByStatus pet petGetByStatusRequest
// Получение питомцев по определенным статусам.
// responses:
//	200:petGetByStatusResponse

// swagger:parameters petGetByStatusRequest
type petGetByStatusRequest struct {
	// in:query
	// required:true
	// items.enum: available, pending, sold
	// name:status
	// schema:
	//	type:array
	// 		items:
	// 			type:string
	Status []string `json:"statuses"`
}

// swagger:response petGetByStatusResponse
type petGetByStatusResponse struct {
	// in:body
	Bodies []models.Pet `json:"body"`
}

// swagger:route POST /pet/{id}/uploadImage pet petUploadImageRequest
// Загрузка фото питомца.
// responses:
//	200: petUploadImageResponse

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// ID питомца
	// in:path
	// required:true
	PetId int64 `json:"id"`

	// Дополнительные метаданные
	// in:formData
	AdditionalMetaData string `json:"metadata"`

	// Файл для загрузки
	// in:formData
	// swagger:file
	Images os.File
}

type Request struct {
	Code    int    `json:"code"`
	Type    string `json:"type"`
	Message string `json:"message"`
}

// swagger:response petUploadImageResponse
type petUploadImageResponse struct {
	R Request `json:"request"`
}

// swagger:route POST /store/order order postOrderRequest
// Размещение заказа на питомца.
// responses:
//	200: postOrderResponse

// swagger:parameters postOrderRequest
type postOrderRequest struct {
	// Заказ для покупки питомца
	// in:body
	Body models.Order `json:"body"`
}

// swagger:response postOrderResponse
type postOrderResponse struct {
	Body models.Order `json:"body"`
}

// swagger:route DELETE /store/order/{orderId} order deleteOrderRequest
// Удаление заказа на питомца.
// responses:
// 	200: deleteOrderResponse

// swagger:parameters deleteOrderRequest
type deleteOrderRequest struct {
	// ID удаляемого заказа
	// in:path
	// required:true
	OrderId int `json:"orderId"`
}

// swagger:response deleteOrderResponse
type deleteOrderResponse struct {
	Body models.Order `json:"order"`
}

// swagger:route GET /store/order/{orderId} order getOrderRequest
// Удаление заказа на питомца.
// responses:
// 	200: getOrderResponse

// swagger:parameters getOrderRequest
type getOrderRequest struct {
	// ID удаляемого заказа
	// in:path
	// required:true
	OrderId int `json:"orderId"`
}

// swagger:response getOrderResponse
type getOrderResponse struct {
	Body models.Order `json:"order"`
}

// swagger:route GET /store/inventory order inventoryOrderRequest
// Возвращает карту статусов в  количество.
// responses:
// 	200: inventoryOrderResponse

// swagger:parameters inventoryOrderRequest
type inventoryOrderRequest struct {
}

// swagger:response inventoryOrderResponse
type inventoryOrderResponse struct {
	Body map[string]int `json:"map"`
}

// swagger:route GET /user/{username} user userGetRequest
// Возвращает пользователя по нику.
// responses:
//	200: userGetResponse

// swagger:parameters userGetRequest
type userGetRequest struct {
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:response userGetResponse
type userGetResponse struct {
	Body models.User `json:"body"`
}

// swagger:route PUT /user/{username} user userPutRequest
// Обновляет пользователя по нику.
// responses:
//	200: userPutResponse

// swagger:parameters userPutRequest
type userPutRequest struct {
	// in:path
	// required:true
	Username string `json:"username"`

	// in:body
	// required:true
	Body models.User `json:"body"`
}

// swagger:response userGetResponse
type userPutResponse struct {
	Body models.User `json:"body"`
}

// swagger:route DELETE /user/{username} user userDeleteRequest
// Удаляет пользователя по нику.
// responses:
//	200: userDeleteResponse

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:response userDeleteResponse
type userDeleteResponse struct {
	Body models.User `json:"body"`
}

// swagger:route POST /user user userCreateRequest
// Создает пользователя.
// responses:
//	200: description: successful operation

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	// in:body
	// required:true
	Body models.User `json:"body"`
}

// swagger:route POST /user/createWithList user userArrayCreateRequest
// Создает массив пользователей.
// responses:
//	200: description: successful operation

// swagger:parameters userArrayCreateRequest
type userArrayCreateRequest struct {
	// in:body
	// required: true
	// schema:
	//	type:array
	// 		items:
	// 			type:object
	Body []models.User `json:"users"`
}

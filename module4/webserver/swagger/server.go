package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"text/template"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/handlers"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := handlers.NewPetController("../pet_repo.json")
	r.Post("/pet", petController.PetCreate)
	r.Get("/pet/{petID}", petController.PetGetByID)
	r.Post("/pet/{petID}", petController.PetUpdateByID)
	r.Delete("/pet/{petID}", petController.PetDeleteByID)
	r.Put("/pet", petController.PetUpdate)
	r.Get("/pet/findByStatus", petController.PetFindByStatus)
	r.Post("/pet/{petID}/uploadImage", petController.PetUploadImage)

	orderController := handlers.NewOrderController("../order_repo.json")
	r.Get("/store/inventory", orderController.Inventory)
	r.Get("/store/order/{orderID}", orderController.OrderGetByID)
	r.Delete("/store/order/{orderID}", orderController.OrderDeleteByID)
	r.Post("/store/order", orderController.OrderCreate)

	userController := handlers.NewUserController("../user_repo.json")
	r.Get("/user/{username}", userController.GetUserByUsername)
	r.Put("/user/{username}", userController.UpdateByUsername)
	r.Delete("/user/{username}", userController.DeleteByUsername)
	r.Post("/user", userController.Create)
	r.Post("/user/createWithList", userController.CreateArray)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	// r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
	// 	http.StripPrefix("/public/", http.FileServer(http.Dir("./"))).ServeHTTP(w, r)
	// })
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

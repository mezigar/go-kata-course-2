package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/models"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/swagger/repo"
)

type OrderController struct {
	storage repo.OrderStorage
}

func NewOrderController(filepath string) *OrderController {
	return &OrderController{storage: *repo.NewOrderStorage(filepath)}
}

func (o *OrderController) OrderCreate(w http.ResponseWriter, r *http.Request) {
	var order models.Order
	err := json.NewDecoder(r.Body).Decode(&order)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order = o.storage.Create(order)

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(order)                           // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) OrderGetByID(w http.ResponseWriter, r *http.Request) {

	orderIDRaw := chi.URLParam(r, "orderID")

	orderID, err := strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                          // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order, err := o.storage.GetByID(orderID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) OrderDeleteByID(w http.ResponseWriter, r *http.Request) {
	orderIDRaw := chi.URLParam(r, "orderID") // получаем petID из chi router

	orderID, err := strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                          // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	if err := o.storage.Delete(orderID); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode("Deleting is successful")

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) Inventory(w http.ResponseWriter, r *http.Request) {
	statusToQuantities := make(map[string]int)
	orders := o.storage.GetList()

	for i := range orders {
		if _, ok := statusToQuantities[orders[i].Status]; !ok {
			statusToQuantities[orders[i].Status] = orders[i].Quantity
		} else {
			statusToQuantities[orders[i].Status] += orders[i].Quantity
		}
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(statusToQuantities)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

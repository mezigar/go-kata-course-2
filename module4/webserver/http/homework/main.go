package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/http/homework/handlers"
)

func main() {

	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Get("/", handlers.HelloUser)

	r.Route("/users", func(r chi.Router) {
		r.Use(handlers.UserCtx)
		r.Get("/", handlers.ListUsers)
		r.Post("/", handlers.PostUser)

		r.Route("/{id}", func(r chi.Router) {
			r.Use(handlers.UserCtx)
			r.Get("/", handlers.GetUser)
		})
	})

	r.Post("/upload", handlers.SaveFile)

	r.Get("/public/example.txt", handlers.UploadFile)

	go func() {
		fmt.Println("Server Started")
		err := http.ListenAndServe(":8080", r)
		if err != nil {
			fmt.Println(err)
			return
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	fmt.Println("Shutdown Server ...")

}

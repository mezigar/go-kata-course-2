package service

import (
	repo "gitlab.com/mezigar/go-kata-course-2/module4/webserver/http/homework/repo"
)

type UserService interface {
	ListUsers() []repo.User
	FindUser(int) repo.User
	AddUser(int, string)
}

type userService struct {
	repo repo.UserRepo
}

func NewService(repo repo.UserRepo) *userService {
	return &userService{repo: repo}
}

func (us *userService) ListUsers() []repo.User {
	return us.repo.ListOfUsers()
}

func (us *userService) FindUser(id int) repo.User {
	return us.repo.FindUserById(id)
}

func (us *userService) AddUser(id int, name string) {
	us.repo.AddNewUser(id, name)
}

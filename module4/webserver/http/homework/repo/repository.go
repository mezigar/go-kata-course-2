package repo

type User struct {
	Id   int
	Name string
}

var Users = []User{
	{
		Id:   1,
		Name: "John",
	},
	{
		Id:   2,
		Name: "Bob",
	},
	{
		Id:   3,
		Name: "Alex",
	},
}

type UserRepo struct {
	users []User
}

type UserRepoContract interface {
	AddNewUser(int, string)
	ListOfUsers() []User
	FindUserById(int) User
}

func NewUserRepo() *UserRepo {
	return &UserRepo{users: Users}
}

func (ur *UserRepo) AddNewUser(Id int, Name string) {
	ur.users = append(ur.users, User{Id: Id, Name: Name})
}

func (ur *UserRepo) ListOfUsers() []User {
	return ur.users
}

func (ur *UserRepo) FindUserById(Id int) User {
	var u User
	for _, user := range ur.users {
		if user.Id == Id {
			u = user
		}
	}
	return u
}

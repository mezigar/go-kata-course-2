package handlers

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/rs/zerolog"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/http/homework/repo"
	"gitlab.com/mezigar/go-kata-course-2/module4/webserver/http/homework/service"
)

var userRepo = repo.NewUserRepo()
var userService = service.NewService(*userRepo)

var logger = zerolog.New(os.Stdout)

func ListUsers(w http.ResponseWriter, r *http.Request) {
	users := userService.ListUsers()
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr).
		Msg("")
	render.JSON(w, r, map[string]interface{}{"users": users})
}

func HelloUser(w http.ResponseWriter, r *http.Request) {
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr).
		Msg("")
	render.JSON(w, r, map[string]string{"message": "Hello, User"})
}

type contextKey string

func (c contextKey) String() string {
	return string(c)
}

var UserContextKey contextKey = "user"

func UserCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var user repo.User
		if id := chi.URLParam(r, "id"); id != "" {
			realId, err := strconv.Atoi(id)
			if err != nil {
				fmt.Println(err)
				return
			}
			user = userService.FindUser(realId)
		}
		ctx := context.WithValue(r.Context(), UserContextKey, user)
		next.ServeHTTP(w, r.WithContext(ctx))

	})
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(repo.User)
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr).
		Str("User", fmt.Sprintf("[%d] %s", user.Id, user.Name)).
		Msg("")
	render.JSON(w, r, map[string]repo.User{"user": user})
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(repo.User)
	userService.AddUser(user.Id, user.Name)
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr).
		Str("User", fmt.Sprintf("[%d] %s", user.Id, user.Name)).
		Msg("")
	render.JSON(w, r, map[string]string{"status": "User is added"})
}

func SaveFile(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("example.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	fileBytes, err := io.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	f, err := os.Create("./public/example.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	_, err = f.Write(fileBytes)
	if err != nil {
		fmt.Println(err)
	}
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr).
		Str("File", fmt.Sprintf("[%d] %s", len(fileBytes), f.Name())).
		Msg("")
	fmt.Fprintf(w, "File downloaded succesfully : ")
	fmt.Fprintf(w, header.Filename)
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "."+r.URL.Path)
	logger.Info().
		Str("Request", r.Method).
		Str("URL", r.URL.Path).
		Str("From", r.RemoteAddr)
	fmt.Fprintf(w, "File uploaded succesfully")
}

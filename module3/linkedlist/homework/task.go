package main

import (
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		tmp := f.end
		tmp.next = newPost
		f.end = newPost
	}
	f.length++

}

// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic("No data in Feed")
	}
	var prev *Post
	curr := f.start

	for curr.publishDate != publishDate {
		if curr.next == nil {
			panic("Not found")
		}
		prev = curr
		curr = curr.next
	}
	prev.next = curr.next
	f.length--
}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.Append(newPost)
	} else {
		var prev *Post
		curr := f.start

		for curr.publishDate < newPost.publishDate {
			prev = curr
			curr = curr.next
		}
		prev.next = newPost
		newPost.next = curr
	}
	f.length++
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Empty feed")
	} else {
		fmt.Println("======================")
		fmt.Printf("Feed Length: %d\n", f.length)

		curr := f.start
		idx := 0
		for idx < f.length {
			fmt.Printf("Item: %d - %v\n", idx, curr)
			curr = curr.next
			idx++
		}
		fmt.Println("======================")
	}
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}

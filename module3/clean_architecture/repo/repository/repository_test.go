package repository

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {
	file, err := os.OpenFile("./data.json", os.O_RDWR, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	rws := io.ReadWriteSeeker(file)

	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:    "find user with id = 1",
			fields:  fields{rws},
			args:    args{id: 1},
			want:    User{ID: 1, Name: "Eugene"},
			wantErr: false,
		},
		{name: "not find user with id = -1",
			fields:  fields{rws},
			args:    args{id: -1},
			want:    User{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			// fmt.Println(r)
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepository.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file, err := os.OpenFile("./data.json", os.O_RDWR, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	rws := io.ReadWriteSeeker(file)

	type fields struct {
		File io.ReadWriteSeeker
	}
	tests := []struct {
		name    string
		fields  fields
		want    []User
		wantErr bool
	}{
		{
			name:   "find correctly",
			fields: fields{File: rws},
			want: []User{
				{ID: 0, Name: "Admin"},
				{ID: 1, Name: "Eugene"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepository.FindAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	file, err := os.OpenFile("./data1.json", os.O_RDWR, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	rws := io.ReadWriteSeeker(file)

	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Save Correctly",
			fields:  fields{File: rws},
			args:    args{User{ID: 2, Name: "Not Eugene"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("UserRepository.Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

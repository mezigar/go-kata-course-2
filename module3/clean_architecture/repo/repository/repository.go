package repository

import (
	"encoding/json"
	"fmt"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	b, err := io.ReadAll(r.File)
	if err != nil {
		fmt.Println(err)
		return err
	}
	var users []User

	if err = json.Unmarshal(b, &users); err != nil {
		fmt.Println(err)
		return err
	}
	users = append(users, record.(User))
	data, err := json.MarshalIndent(users, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	_, err = r.File.Seek(0, 0)
	if err != nil {
		fmt.Println(err)
		return err
	}

	_, err = r.File.Write(data)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	b, err := io.ReadAll(r.File)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var users []User
	_ = json.Unmarshal(b, &users)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return nil, err
	// }
	fmt.Println(users)
	for _, user := range users {
		fmt.Printf("User.ID: %d - id: %d\n", user.ID, id)
		if user.ID == id {
			return user, nil
		}
	}
	return User{}, fmt.Errorf("No user with such Id")
}

func (r *UserRepository) FindAll() ([]User, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	b, err := io.ReadAll(r.File)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var users []User
	err = json.Unmarshal(b, &users)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return users, nil
}

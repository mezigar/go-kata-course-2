package main

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/repo/repository"
)

func main() {
	file, err := os.OpenFile("./data.json", os.O_RDWR, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	rw := io.ReadWriteSeeker(file)
	uR := repository.NewUserRepository(rw)
	// data := repository.User{
	// 	ID:   1,
	// 	Name: "Eugene",
	// }
	// err = uR.Save(data)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	Eugene, err := uR.Find(1)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(Eugene)

	// users, err := uR.FindAll()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// for _, user := range users {
	// 	fmt.Println(user)
	// }

}

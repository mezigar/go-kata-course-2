package main

import (
	"fmt"

	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/model"
	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/repo"
	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/service"
)

func main() {
	repo := repo.FileTaskRepository{FilePath: "./data.json"}
	service := service.NewService(&repo)
	if err := service.CreateTodo("First todo!"); err != nil {
		fmt.Println(err)
	}
	if err := service.CompleteTodo(model.Todo{ID: 1, Title: "First todo completed"}); err != nil {
		fmt.Println(err)
	}
}

package repo

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

// Repository layer

// Task represents a to-do task
type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	IsDone      bool   `json:"status"`
}

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
	SaveTasks(tasks []Task) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewRepository(filepath string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: filepath}
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		fmt.Println(err)
		return err
	}
	for _, task := range tasks {
		if task.ID == id {
			tasks = append(tasks[:task.ID], tasks[task.ID+1:]...)
			_ = tasks
			return nil
		}
	}
	return fmt.Errorf("No task with such id %d", id)
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	var tasks []Task

	file, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE, 0755)

	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, fmt.Errorf("No task with such id: %d ", id)
}

func (repo *FileTaskRepository) SaveTasks(tasks []Task) error {
	data, err := json.MarshalIndent(tasks, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	f, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	if _, err = f.Write(data); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	i := 0
	for _, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
		i++
	}
	if i >= len(tasks) {
		return Task{}, fmt.Errorf("No todo with such id %d", task.ID)
	}
	if err := repo.SaveTasks(tasks); err != nil {
		return Task{}, err
	}

	return task, nil
}

package service

import (
	"reflect"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/model"
	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/repo"
)

func Test_todoService_ListTodos(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &todoService{
				repo: tt.fields.repo,
			}
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.ListTodos() = %v, want %v", got, tt.want)
			}
		})
	}
}

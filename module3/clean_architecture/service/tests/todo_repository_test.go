package tests

import (
	"reflect"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/repo"
)

func TestFileTaskRepository_GetTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []repo.Task
		wantErr bool
	}{
		{name: "Correct test",
			fields: fields{FilePath: "./data_repo.json"},
			want: []repo.Task{
				{ID: 1, Title: "First todo", Description: "", IsDone: false},
				{ID: 2, Title: "Second todo", Description: "Very important description", IsDone: false},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FileTaskRepository.GetTasks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{
			name:    "First to do",
			fields:  fields{FilePath: "./data_repo.json"},
			args:    args{id: 1},
			want:    repo.Task{ID: 1, Title: "First todo", Description: "", IsDone: false},
			wantErr: false,
		},
		{
			name:    "Not existing todo",
			fields:  fields{FilePath: "./data.json"},
			args:    args{id: 3},
			want:    repo.Task{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTask(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.GetTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FileTaskRepository.GetTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_SaveTasks(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		tasks []repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "Save correctly",
			fields: fields{FilePath: "./data_repo_for_save.json"},
			args: args{[]repo.Task{{ID: 1, Title: "First todo resaved", IsDone: false, Description: ""},
				{ID: 2, Title: "Second todo resaved", IsDone: false, Description: "Very important description"}}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.SaveTasks(tt.args.tasks); (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.SaveTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileTaskRepository_CreateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{name: "Correct save",
			fields:  fields{FilePath: "./data_repo_for_save.json"},
			args:    args{repo.Task{ID: 3, Title: "Created task", IsDone: false, Description: ""}},
			want:    repo.Task{ID: 3, Title: "Created task", IsDone: false, Description: ""},
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.CreateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FileTaskRepository.CreateTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		task repo.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    repo.Task
		wantErr bool
	}{
		{
			name:    "Correct update",
			fields:  fields{FilePath: "./data_repo_for_save.json"},
			args:    args{task: repo.Task{ID: 1, Title: "First todo is done", Description: "hopefully", IsDone: true}},
			want:    repo.Task{ID: 1, Title: "First todo is done", Description: "hopefully", IsDone: true},
			wantErr: false,
		},
		{
			name:    "Incorrect update",
			fields:  fields{FilePath: "./data_repo_for_save.json"},
			args:    args{task: repo.Task{ID: 5, Title: "Not existing todo", Description: "", IsDone: false}},
			want:    repo.Task{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.UpdateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FileTaskRepository.UpdateTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Correct deleting",
			fields:  fields{FilePath: "./data_repo_for_save.json"},
			args:    args{id: 2},
			wantErr: false,
		},
		{
			name:    "Incorrect deleting",
			fields:  fields{FilePath: "./data_repo_for_save.json"},
			args:    args{id: 9},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.DeleteTask(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("FileTaskRepository.DeleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

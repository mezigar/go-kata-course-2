package tests

import (
	"reflect"
	"testing"

	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/model"
	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/repo"
	"gitlab.com/mezigar/go-kata-course-2/module3/clean_architecture/service/service"
)

func Test_todoService_ListTodos(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		{
			name:   "correct list",
			fields: fields{repo: repo.NewRepository("./data_service.json")},
			want: []model.Todo{
				{
					ID:          1,
					IsDone:      false,
					Title:       "First todo",
					Description: "",
				},
				{
					ID:          2,
					IsDone:      true,
					Title:       "Second todo",
					Description: "So good and tasty"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.ListTodos() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_CreateTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{name: "creating",
			fields:  fields{repo: repo.NewRepository("./data_service.json")},
			args:    args{title: "New todo"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.CreateTodo(tt.args.title); (err != nil) != tt.wantErr {
				t.Errorf("todoService.CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_CompleteTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Correct compete",
			fields:  fields{repo: repo.NewRepository("./data_service.json")},
			args:    args{model.Todo{ID: 1, Title: "First todo", Description: "", IsDone: false}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("todoService.CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	type fields struct {
		repo repo.TaskRepository
	}
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "Correct deleting",
			fields: fields{repo: repo.NewRepository("./data_service_for_deleting.json")},
			args: args{
				todo: model.Todo{
					ID:          1,
					IsDone:      true,
					Title:       "First todo",
					Description: "",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("todoService.RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

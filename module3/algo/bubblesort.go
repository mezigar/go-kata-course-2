package algo

func BubbleSort(data []int) []int {
	swaped := true
	i := 1
	for swaped {
		swaped = false
		for j := 0; j < len(data)-1; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swaped = true
			}
		}
		i++
	}
	return data
}

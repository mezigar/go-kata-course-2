package medium

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root.Left != nil {
		root.Left = removeLeafNodes(root.Left, target)
	}
	if root.Right != nil {
		root.Right = removeLeafNodes(root.Right, target)
	}
	if root.Val == target && root.Left == nil && root.Right == nil {
		return nil
	}
	return root
}

var _ *TreeNode = removeLeafNodes(&TreeNode{}, 5)

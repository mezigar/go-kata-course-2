package medium

func xorQueries(arr []int, queries [][]int) []int {
	ans := make([]int, 0, len(queries))
	for i := range queries {
		xor := 0
		for j := queries[i][0]; j <= queries[i][1]; j++ {
			xor ^= arr[j]
		}
		ans = append(ans, xor)
	}
	return ans
}

var _ []int = xorQueries([]int{1, 3, 5, 7}, [][]int{{1, 2}, {2, 3}})

package medium

func processQueries(queries []int, m int) []int {
	p := generateP(m)
	res := make([]int, len(queries))

	for i := 0; i < len(queries); i++ {
		query := queries[i]
		index := index(p, query)
		res[i] = index

		relocate(p, index)
	}

	return res
}

func generateP(m int) []int {
	res := make([]int, m)

	for i := 0; i < m; i++ {
		res[i] = i + 1
	}

	return res
}

func index(arr []int, val int) int {
	for i, v := range arr {
		if v == val {
			return i
		}
	}

	return -1
}

func relocate(arr []int, index int) {
	val := arr[index]

	for index > 0 {
		arr[index] = arr[index-1]
		index--
	}

	arr[0] = val
}

var _ []int = processQueries([]int{3, 1, 2, 1}, 5)

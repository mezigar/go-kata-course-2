package medium

func sortTheStudents(score [][]int, k int) [][]int {
	for i := 0; i < len(score)-1; i++ {
		for j := i; j < len(score); j++ {
			if score[i][k] < score[j][k] {
				score[i], score[j] = score[j], score[i]
			}
		}
	}
	return score
}

var _ [][]int = sortTheStudents([][]int{{3, 4}, {5, 5}}, 1)

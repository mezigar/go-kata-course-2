package medium

func maxSum(grid [][]int) int {
	sum := 0

	for i := 0; i < len(grid)-2; i++ {
		for j := 0; j < len(grid[0])-2; j++ {
			curSum := grid[i][j] + grid[i][j+1] + grid[i][j+2] + grid[i+1][j+1] + grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2]
			if curSum > sum {
				sum = curSum
			}
		}
	}

	return sum
}

var _ int = maxSum([][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}})

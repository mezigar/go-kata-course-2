package medium

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	ans := make([]bool, 0, len(l))
	for i := range l {
		subArr := nums[l[i] : r[i]+1]
		if isArithm(subArr) {
			ans = append(ans, true)
		} else {
			ans = append(ans, false)
		}
	}
	return ans
}

func isArithm(nums []int) bool {
	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] > nums[j] {
				nums[i], nums[j] = nums[j], nums[i]
			}
		}
	}
	prevDif := 0
	if len(nums) > 1 {
		prevDif = nums[1] - nums[0]
	} else {
		return true
	}

	for i := 1; i < len(nums)-1; i++ {
		if prevDif != nums[i+1]-nums[i] {
			return false
		}
	}
	return true
}

var _ []bool = checkArithmeticSubarrays([]int{4, 6, 5, 9, 3, 7}, []int{0, 0, 2}, []int{2, 3, 5})

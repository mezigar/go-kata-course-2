package medium

type pair struct {
	sum, count int
}

func solve(root *TreeNode, ans *int) pair {
	if root == nil {
		return pair{0, 0}
	}
	l := solve(root.Left, ans)
	r := solve(root.Right, ans)

	sum := l.sum + r.sum + root.Val
	count := l.count + r.count + 1
	if sum/count == root.Val {
		*ans++
	}
	return pair{sum, count}
}

func averageOfSubtree(root *TreeNode) int {
	var ans int
	solve(root, &ans)
	return ans
}

var _ int = averageOfSubtree(&TreeNode{})

package medium

func bstToGst(root *TreeNode) *TreeNode {
	var lst []*TreeNode
	var returnNil func(*TreeNode)
	returnNil = func(root *TreeNode) {
		if root == nil {
			return
		}
		returnNil(root.Right)
		lst = append(lst, root)
		returnNil(root.Left)
	}

	returnNil(root)
	prefixSum := 0

	for _, node := range lst {
		oldValue := node.Val
		node.Val += prefixSum
		prefixSum += oldValue
	}
	return root

}

var _ *TreeNode = bstToGst(&TreeNode{})

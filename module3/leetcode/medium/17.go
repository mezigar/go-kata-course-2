package medium

func countPoints(points [][]int, queries [][]int) []int {
	ans := make([]int, 0, len(queries))
	for qidx := range queries {
		inCircle := 0
		for pidx := range points {
			if (points[pidx][0]-queries[qidx][0])*(points[pidx][0]-queries[qidx][0])+(points[pidx][1]-queries[qidx][1])*(points[pidx][1]-queries[qidx][1]) <= queries[qidx][2]*queries[qidx][2] {
				inCircle++
			}
		}
		ans = append(ans, inCircle)
	}
	return ans
}

var _ []int = countPoints([][]int{{1, 2}, {2, 3}}, [][]int{{0, 0, 3}, {1, 1, 2}})

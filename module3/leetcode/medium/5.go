package medium

func pairSum(head *ListNode) int {
	mid := middleNode(head)

	var prev *ListNode

	for head != nil {
		next := head.Next
		head.Next = prev
		prev = head
		head = next

		if prev == mid {
			break
		}
	}
	res := 0
	for head != nil {
		if head.Val+prev.Val > res {
			res = head.Val + prev.Val
		}

		head = head.Next
		prev = prev.Next
	}

	return res

}

func middleNode(head *ListNode) *ListNode {
	slow, fast := head, head
	for slow.Next != nil && fast.Next.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	return slow
}

var _ int = pairSum(&ListNode{})

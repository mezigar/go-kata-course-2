package medium

func groupThePeople(groupSizes []int) [][]int {
	res := make([][]int, 0)
	groups := make(map[int][]int)

	for uid, size := range groupSizes {
		groups[size] = append(groups[size], uid)
		if len(groups[size]) == size {
			res = append(res, groups[size])
			groups[size] = nil
		}
	}
	return res
}

var _ [][]int = groupThePeople([]int{3, 3, 3, 3, 3, 1, 3})

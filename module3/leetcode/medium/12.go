package medium

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	res := list1
	currNode := res

	var prevNode *ListNode = nil
	for i := 0; i < a; i++ {
		prevNode = currNode
		currNode = currNode.Next
	}

	prevNode.Next = nil

	prevNode = nil

	for i := a; i <= b; i++ {
		prevNode = currNode
		currNode = currNode.Next
	}

	if prevNode.Next != nil {
		prevNode.Next = nil
	}

	for res.Next != nil {
		res = res.Next
	}

	res.Next = list2

	for res.Next != nil {
		res = res.Next
	}

	res.Next = currNode

	return list1
}

var _ *ListNode = mergeInBetween(&ListNode{}, 0, 0, &ListNode{})

package medium

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	var mergedList, tail *ListNode
	sum := 0
	for head != nil {
		sum += head.Val
		if head.Val == 0 && sum != 0 {
			newNode := &ListNode{Val: sum}
			if mergedList == nil {
				mergedList = newNode
				tail = newNode
			} else {
				tail.Next = newNode
				tail = newNode
			}
			sum = 0
		}
		head = head.Next
	}
	return mergedList
}

var _ *ListNode = mergeNodes(&ListNode{})

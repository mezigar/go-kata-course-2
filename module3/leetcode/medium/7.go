package medium

func balanceBST(root *TreeNode) *TreeNode {
	nodes := []*TreeNode{}
	dfs(root, &nodes)
	return balance(nodes)
}

func balance(nodes []*TreeNode) *TreeNode {
	n := len(nodes)
	if n == 0 {
		return nil
	}
	if n == 1 {
		nodes[0].Left, nodes[0].Right = nil, nil
		return nodes[0]
	}
	newRoot := nodes[n/2]
	newRoot.Left = balance(nodes[:n/2])
	newRoot.Right = balance(nodes[n/2+1:])
	return newRoot
}

func dfs(node *TreeNode, nodes *[]*TreeNode) {
	if node == nil {
		return
	}
	if node.Left != nil {
		dfs(node.Left, nodes)
	}
	*nodes = append(*nodes, node)
	if node.Right != nil {
		dfs(node.Right, nodes)
	}
}

var _ *TreeNode = balanceBST(&TreeNode{})

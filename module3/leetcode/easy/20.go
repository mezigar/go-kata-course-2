package easy

func kidsWithCandies(candies []int, extraCandies int) []bool {
	result := make([]bool, 0, len(candies))
	maxCandies := 0
	for _, candy := range candies {
		if candy > maxCandies {
			maxCandies = candy
		}
	}

	for _, candy := range candies {
		if candy+extraCandies >= maxCandies {
			result = append(result, true)
		} else {
			result = append(result, false)
		}
	}
	return result
}

var _ []bool = kidsWithCandies([]int{1, 2, 3}, 4)

package easy

func createTargetArray(nums []int, index []int) []int {
	ans := make([]int, len(nums))
	for i := range index {
		curIdx := index[i]
		if i != curIdx {
			for mvIdx := i; mvIdx > curIdx; mvIdx-- {
				ans[mvIdx] = ans[mvIdx-1]
			}
		}
		ans[curIdx] = nums[i]
	}
	return ans
}

var _ []int = createTargetArray([]int{1, 2, 3, 4, 0}, []int{0, 1, 2, 3, 0})

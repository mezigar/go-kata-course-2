package easy

func buildArray(nums []int) []int {
	ans := make([]int, len(nums))
	for i := range nums {
		ans[i] = nums[nums[i]]
	}
	return ans
}

var _ []int = buildArray([]int{1, 0, 2})

package easy

func defangIPaddr(address string) string {
	ans := ""
	for _, char := range address {
		if char == rune('.') {
			ans += "[.]"
		} else {
			ans += string(char)
		}
	}
	return ans
}

var _ string = defangIPaddr("1.1.1.1")

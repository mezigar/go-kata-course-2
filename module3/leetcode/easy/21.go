package easy

func subtractProductAndSum(n int) int {
	product, sum := 1, 0
	for n > 0 {
		product *= n % 10
		sum += n % 10
		n /= 10
	}
	return product - sum
}

var _ int = subtractProductAndSum(2341)

package easy

func maximumWealth(accounts [][]int) int {
	maxWealth := 0
	for _, account := range accounts {
		tmpWealth := 0
		for _, bankDeposit := range account {
			tmpWealth += bankDeposit
		}
		if tmpWealth > maxWealth {
			maxWealth = tmpWealth
		}
	}
	return maxWealth
}

func InitAccounts() [][]int {
	var accounts = make([][]int, 2)

	for i := 0; i < 2; i++ {
		accounts[i] = make([]int, 3)

		for j := 0; j < 3; j++ {
			accounts[i][j] = i + j*i
		}
	}
	return accounts
}

var _ = maximumWealth(InitAccounts())

package easy

func interpret(command string) string {
	ans := ""
	for i := range command {
		if string(command[i]) == "G" {
			ans += "G"
		} else if string(command[i]) == "(" {
			if string(command[i+1]) == "a" {
				ans += "al"
			} else {
				ans += "o"
			}
		}
	}
	return ans
}

var _ string = interpret("G()(al)")

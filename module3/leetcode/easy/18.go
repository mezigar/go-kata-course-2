package easy

func differenceOfSum(nums []int) int {
	sumDigits, sum := 0, 0
	for _, num := range nums {
		sumDigits += sumOfDigits(num)
		sum += num
	}
	if sum > sumDigits {
		return sum - sumDigits
	} else {
		return sumDigits - sum
	}
}

func sumOfDigits(num int) int {
	sum := 0
	for num > 0 {
		sum += num % 10
		num /= 10
	}
	return sum
}

var _ int = differenceOfSum([]int{1, 15, 9})

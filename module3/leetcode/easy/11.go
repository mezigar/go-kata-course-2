package easy

func runningSum(nums []int) []int {
	sums := make([]int, 0, len(nums))
	sum := 0
	for i := range nums {
		sum += nums[i]
		sums = append(sums, sum)
	}
	return sums
}

var _ []int = runningSum([]int{1, 3, 5})

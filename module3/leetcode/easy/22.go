package easy

func smallerNumbersThanCurrent(nums []int) []int {
	ans := make([]int, 0, len(nums))
	for i := range nums {
		val := 0
		for j := range nums {
			if nums[j] < nums[i] && i != j {
				val++
			}
		}
		ans = append(ans, val)
	}
	return ans
}

var _ []int = smallerNumbersThanCurrent([]int{1, 2, 3, 1})

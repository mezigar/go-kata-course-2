package easy

func balancedStringSplit(s string) int {
	rcnt, lcnt := 0, 0
	res := 0
	for _, ch := range s {
		if ch == 'L' {
			lcnt++
		} else {
			rcnt++
		}
		if lcnt == rcnt {
			res++
			lcnt, rcnt = 0, 0
		}
	}
	return res
}

var _ int = balancedStringSplit("LRLR")

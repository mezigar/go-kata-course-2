package easy

func numJewelsInStones(jewels string, stones string) int {
	cnt := 0
	for i := range jewels {
		for j := range stones {
			if jewels[i] == stones[j] {
				cnt++
			}
		}
	}
	return cnt
}

var _ int = numJewelsInStones("a", "AaaA")

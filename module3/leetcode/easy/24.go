package easy

func decode(encoded []int, first int) []int {
	res := make([]int, 0, len(encoded)+1)
	res = append(res, first)
	for i, el := range encoded {
		res = append(res, res[i]^el)
	}
	return res
}

var _ []int = decode([]int{1, 2, 3}, 1)

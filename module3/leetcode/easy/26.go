package easy

func decompressRLElist(nums []int) []int {
	ans := make([]int, 0)
	for i := 0; i < len(nums); i += 2 {
		for j := nums[i]; j > 0; j-- {
			ans = append(ans, nums[i+1])
		}
	}
	return ans
}

var _ []int = decompressRLElist([]int{1, 2, 3, 4})

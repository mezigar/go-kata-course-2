package easy

func shuffle(nums []int, n int) []int {
	data := make([]int, 0, len(nums))
	for i := 0; i < n; i++ {
		data = append(data, nums[i])
		data = append(data, nums[i+n])
	}
	return data
}

var _ []int = shuffle([]int{1, 3, 2, 4}, 2)

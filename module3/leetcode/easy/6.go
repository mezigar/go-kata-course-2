package easy

func uniqueMorseRepresentations(words []string) int {
	code := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	tmp := make(map[string]int)
	for _, w := range words {
		temp := ""
		for _, c := range w {
			temp += code[c-'a']
		}
		tmp[temp]++
	}
	return len(tmp)
}

var _ int = uniqueMorseRepresentations([]string{"a"})

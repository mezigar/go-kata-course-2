package easy

func minimumSum(num int) int {
	digits := make([]int, 0, 4)
	for num > 0 {
		digits = append(digits, num%10)
		num /= 10
	}

	for i := 0; i < len(digits); i++ {
		for j := 0; j < len(digits); j++ {
			if digits[i] < digits[j] {
				digits[j], digits[i] = digits[i], digits[j]
			}
		}
	}
	return 10*(digits[0]+digits[1]) + digits[2] + digits[3]
}

var _ int = minimumSum(2932)

package easy

func convertTemperature(celsius float64) []float64 {
	return []float64{celsius + 273.15, celsius*1.80 + 32.00}
}

var _ []float64 = convertTemperature(32.00)

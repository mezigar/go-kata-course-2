package easy

func numIdenticalPairs(nums []int) int {
	pairs := 0
	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				pairs++
			}
		}
	}
	return pairs
}

var _ int = numIdenticalPairs([]int{1, 2, 3, 1, 1, 3})

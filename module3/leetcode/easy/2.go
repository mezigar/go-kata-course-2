package easy

func getConcatenation(nums []int) []int {
	ans := make([]int, 2*len(nums))
	for i := range nums {
		ans[i] = nums[i]
		ans[len(nums)+i] = nums[i]
	}
	return ans
}

var _ []int = getConcatenation([]int{1, 2, 1})

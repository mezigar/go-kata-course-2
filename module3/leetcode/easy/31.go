package easy

func sortPeople(names []string, heights []int) []string {
	for i := 0; i < len(heights)-1; i++ {
		for j := i + 1; j < len(heights); j++ {
			if heights[i] < heights[j] {
				heights[i], heights[j] = heights[j], heights[i]
				names[i], names[j] = names[j], names[i]
			}
		}
	}
	return names
}

var _ []string = sortPeople([]string{"Alice", "Bob"}, []int{155, 175})

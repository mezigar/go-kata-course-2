package easy

func countDigits(num int) int {
	ans := 0
	cpy_num := num
	for num > 0 {
		digit := num % 10
		if cpy_num%digit == 0 {
			ans++
		}
		num /= 10
	}
	return ans
}

var _ int = countDigits(7)

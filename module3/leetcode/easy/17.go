package easy

func mostWordsFound(sentences []string) int {
	maxSentences := 0
	for _, sentence := range sentences {
		tmpSent := 0
		for _, ch := range sentence {
			if ch == rune(' ') {
				tmpSent++
			}
		}
		if tmpSent > maxSentences {
			maxSentences = tmpSent
		}
	}
	return maxSentences + 1
}

var _ int = mostWordsFound([]string{"a a", "aa"})

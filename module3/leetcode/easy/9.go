package easy

func finalValueAfterOperations(operations []string) int {
	sum := 0
	for _, operation := range operations {
		if operation == "X++" || operation == "++X" {
			sum++
		} else {
			sum--
		}
	}
	return sum
}

var _ int = finalValueAfterOperations([]string{"X++", "++X", "--X"})

package easy

func abs(x int) int {
	if x < 0 {
		return -1 * x
	} else {
		return x
	}
}
func countGoodTriplets(arr []int, a int, b int, c int) int {
	ans := 0
	for i := range arr {
		for j := range arr {
			for k := range arr {
				if abs(arr[i]-arr[j]) <= a && abs(arr[j]-arr[k]) <= b && abs(arr[i]-arr[k]) <= c && i < j && j < k {
					ans++
				}
			}
		}
	}
	return ans
}

var _ int = countGoodTriplets([]int{1, 2, 3}, 1, 2, 3)

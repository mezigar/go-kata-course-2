package airconditioner

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type RealAirConditioner struct {
	temp int
	isOn bool
}

func (r *RealAirConditioner) RealTurnOff() {
	r.isOn = false

}

func (r *RealAirConditioner) RealTurnOn() {
	r.isOn = true

}

func (r *RealAirConditioner) RealSetTemperature(temp int) {
	r.temp = temp
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (Adapter AirConditionerAdapter) TurnOff() {
	Adapter.airConditioner.RealTurnOff()
}

func (Adapter AirConditionerAdapter) TurnOn() {
	Adapter.airConditioner.RealTurnOn()
}

func (Adapter AirConditionerAdapter) SetTemperature(temp int) {
	Adapter.airConditioner.RealSetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{adapter: &AirConditionerAdapter{
		airConditioner: &RealAirConditioner{},
	},
		authenticated: authenticated,
	}
}

func (Proxy AirConditionerProxy) TurnOff() {
	if Proxy.authenticated {
		fmt.Println("Turning off the air conditioner")
		Proxy.adapter.TurnOff()
	} else {
		fmt.Println("Access denied: auth required to turn off the air conditioner")
	}
}

func (Proxy AirConditionerProxy) TurnOn() {
	if Proxy.authenticated {
		fmt.Println("Turning on the air conditioner")
		Proxy.adapter.TurnOn()
	} else {
		fmt.Println("Access denied: auth required to turn on the air conditioner")
	}
}

func (Proxy AirConditionerProxy) SetTemperature(temp int) {
	if Proxy.authenticated {
		fmt.Printf("Setting air conditioner temperature to %d\n", temp)
		Proxy.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied auth reqired to set temperature of the air conditioneer")
	}
}

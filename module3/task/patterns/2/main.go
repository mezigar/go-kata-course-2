package main

import conditioner "gitlab.com/mezigar/go-kata-course-2/module3/task/patterns/2/air_conditioner"

func main() {
	airConditioner := conditioner.NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = conditioner.NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}

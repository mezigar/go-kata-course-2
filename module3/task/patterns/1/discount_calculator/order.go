package discountcalculator

type Order struct {
	Name     string
	Price    float64
	Quantity int
}

func (o Order) GetName() string {
	return o.Name
}

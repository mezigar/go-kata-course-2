package discountcalculator

import "fmt"

type PricingStrategy interface {
	Calculate(Order) float64
}

type RegularPricing struct{}

func (r RegularPricing) Calculate(order Order) float64 {
	full_price := order.Price * float64(order.Quantity)
	fmt.Printf("Total cost with %T strategy: %v\n", r, full_price)
	return full_price
}

type SalePricing struct {
	Title    string
	Discount float64
}

func (s SalePricing) Calculate(order Order) float64 {
	discount_price := order.Price * float64(order.Quantity) * (1 - s.Discount)
	fmt.Printf("Total cost with %T strategy: %v\n", s, discount_price)
	return discount_price
}

func (s SalePricing) GetName() string {
	return s.Title
}

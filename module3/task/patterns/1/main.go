package main

import (
	discountcalculator "gitlab.com/mezigar/go-kata-course-2/module3/task/patterns/1/discount_calculator"
)

func main() {
	order := discountcalculator.Order{
		Name:     "Watermelon",
		Price:    100.0,
		Quantity: 2,
	}
	strategies := []discountcalculator.PricingStrategy{discountcalculator.RegularPricing{},
		discountcalculator.SalePricing{Title: "Regulary client discount", Discount: 0.05},
		discountcalculator.SalePricing{Title: "Early Spring discount", Discount: 0.1},
		discountcalculator.SalePricing{Title: "VIP discount", Discount: 0.15}}

	for _, strategy := range strategies {
		strategy.Calculate(order)
	}
}

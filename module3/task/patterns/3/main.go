package main

import (
	"fmt"

	weatherapi "gitlab.com/mezigar/go-kata-course-2/module3/task/patterns/3/weather_api"
)

func main() {
	weatherFacade := weatherapi.NewWeatherFacade("0995b5f412msh6c3f3e2f25480b2p19d877jsn2af1ce7fd19c")
	cities := []string{"Москва", "Санкт-Петербуг", "Казань", "Якутск"}

	for _, city := range cities {
		temp, hum, wind := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %v\n", temp)
		fmt.Printf("Humidity in "+city+": %v\n", hum)
		fmt.Printf("Wind speed in "+city+": %v\n\n", wind)
	}
}

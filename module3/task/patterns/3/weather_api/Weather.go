package weatherapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type WeatherAPI interface {
	GetTemperature(location string) float64
	GetHumidity(location string) int
	GetWindSpeed(location string) float64
}

type OpenWeatherAPI struct {
	apiKey string
}

type WeatherData struct {
	Coords struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	}
	Main struct {
		Temp       float64 `json:"temp"`
		FeelsLike  float64 `json:"feels_like"`
		Temp_Min   float64 `json:"temp_min"`
		Temp_Max   float64 `json:"temp_max"`
		Pressure   int     `json:"pressure"`
		Humidity   int     `json:"humidity"`
		Visibility int     `json:"visibility"`
	}
	Wind struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
	}
}

var NameToCoords map[string][2]float64 = map[string][2]float64{
	"Москва":          {55.76, 37.62},
	"Санкт-Петербург": {59.95, 30.32},
	"Казань":          {55.79, 49.11},
	"Якутск":          {62.03, 129.73},
}

func NewAPI(apikey string) *OpenWeatherAPI {
	return &OpenWeatherAPI{apiKey: apikey}
}
func (o OpenWeatherAPI) GetRequstToAPI(lat, lon float64) WeatherData {
	url := fmt.Sprintf("https://open-weather13.p.rapidapi.com/city/latlon/%v/%v", lat, lon)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("X-RapidAPI-Key", o.apiKey)
	req.Header.Add("X-RapidAPI-Host", "open-weather13.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	defer res.Body.Close()

	body, _ := io.ReadAll(res.Body)

	var data WeatherData
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println(err)
	}

	return data
}

func (o *OpenWeatherAPI) GetTemperature(location string) float64 {
	coords := NameToCoords[location]
	data := o.GetRequstToAPI(coords[0], coords[1])

	return data.Main.Temp
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) float64 {
	coords := NameToCoords[location]
	data := o.GetRequstToAPI(coords[0], coords[1])

	return data.Wind.Speed
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	coords := NameToCoords[location]
	data := o.GetRequstToAPI(coords[0], coords[1])

	return data.Main.Humidity
}

type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (float64, int, float64) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

package task

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestDoubleLinkedList_Len(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second

	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "len = 2",
			fields: fields{
				head: &head,
				curr: &head,
				tail: head.next,
				len:  2,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("DoubleLinkedList.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoadData(t *testing.T) {
	var DLL DoubleLinkedList = DoubleLinkedList{}
	err := DLL.LoadData("./test.json")
	if err != nil {
		t.Errorf("Error in data loading %s", err)
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "First node",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want: &head,
		},
		{
			name: "Second node",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			want: &second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Not nil Next",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want: &second,
		},
		{
			name: "nil next",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Not nil prev",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			want: &head,
		},
		{
			name: "nil prev",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	type args struct {
		n int
		c Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "correct insert after head",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			args: args{
				n: 1,
				c: Commit{
					Message: "Third commit",
					UUID:    "3",
					Date:    time.Now().Add(3 * time.Hour),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("DoubleLinkedList.Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_InsertAtEnd(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		c Commit
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "correct insertion",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			args: args{
				c: Commit{
					Message: "Third commit",
					UUID:    "3",
					Date:    time.Now().Add(3 * time.Hour),
				}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			d.InsertAtEnd(tt.args.c)
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Corect deleting",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			args:    args{n: 1},
			wantErr: false,
		},
		{
			name: "Incorrect deleting",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			args:    args{n: 3},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("DoubleLinkedList.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Correct deleting from tail",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			wantErr: false,
		},
		{
			name: "Correct deleting from head",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DoubleLinkedList.DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{
			name: "1st index",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "2nd index",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			want:    2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("DoubleLinkedList.Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DoubleLinkedList.Index() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Correct pop",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			want: &head,
		},
		{
			name: "Incorrect pop",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Correct pop",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want: head.next,
		},
		{
			name: "Incorrect pop",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Shift() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		uuID string
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "Correct searchUUID",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			args: args{uuID: "1"},
			want: &head,
		},
		{
			name: "Incorrect searchUUID",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			args: args{uuID: "3"},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.SearchUUID(tt.args.uuID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		message string
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "Correct Search",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			args: args{
				message: "First commit",
			},
			want: &head,
		},
		{
			name: "Incorrect Search",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: head.next,
				len:  2,
			},
			args: args{
				message: "Bla-bla",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Search(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = Node{
		data: &Commit{
			Message: "First commit",
			Date:    time.Now(),
			UUID:    "1",
		},
		prev: nil,
	}
	var second = Node{
		data: &Commit{
			Message: "Second commit",
			Date:    time.Now().Add(3 * time.Hour),
			UUID:    "2",
		},
		prev: &head,
		next: nil,
	}
	head.next = &second

	want := DoubleLinkedList{
		head: &second,
		tail: &head,
		curr: &head,
		len:  2,
	}
	tests := []struct {
		name   string
		fields fields
		want   *DoubleLinkedList
	}{
		{
			name: "Reversing",
			fields: fields{
				head: &head,
				tail: head.next,
				curr: &head,
				len:  2,
			},
			want: &want,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Reverse(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleLinkedList.Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func readData(n int, path string) [][]Commit {
	data := make([][]Commit, n)
	jsonFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}

	for i := 0; i < n; i++ {
		data[i] = make([]Commit, 4000)
		if err = json.Unmarshal(byteValue, &data[i]); err != nil {
			fmt.Println(err)
		}
	}
	return data
}

func BenchmarkSort(b *testing.B) {
	data := readData(b.N, "./test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		QuickSort(data[i])
	}

}

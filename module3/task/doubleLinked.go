package task

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	InsertAtEnd(c Commit)
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	jsonFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
		return err
	}

	var Commits []Commit
	err = json.Unmarshal(byteValue, &Commits)
	if err != nil {
		fmt.Println(err)
		return err
	}
	QuickSort(Commits)
	for _, commit := range Commits {
		d.InsertAtEnd(commit)
	}
	return nil
}

func QuickSort(Commits []Commit) []Commit {
	if len(Commits) <= 1 {
		return Commits
	}

	median := Commits[rand.Intn(len(Commits))]

	low_part := make([]Commit, 0, len(Commits))
	high_part := make([]Commit, 0, len(Commits))
	middle_part := make([]Commit, 0, len(Commits))

	for _, commit := range Commits {
		switch {
		case commit.Date.Before(median.Date):
			low_part = append(low_part, commit)
		case commit.Date.After(median.Date):
			high_part = append(high_part, commit)
		case commit.Date == median.Date:
			middle_part = append(middle_part, commit)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

func (d *DoubleLinkedList) TraverseDLL(n int) {
	fmt.Printf("Doubly Linked List: ")
	tmp := d.head
	i := 0
	for tmp != nil && i < n {
		fmt.Printf("[%v]-%s: %s", tmp.data.Date, tmp.data.UUID, tmp.data.Message)
		tmp = tmp.next
		i++
	}
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	return d.curr.next

}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n > d.len {
		return fmt.Errorf("Length is less than %d", n)
	}
	cur := d.head
	newNode := Node{data: &c}
	if n == 1 {
		newNode.next = cur
		cur.prev = &newNode
		d.head = &newNode
	} else {
		for i := 0; i < n; i++ {
			cur = cur.next
		}
		newNode.next = cur.next
		newNode.prev = cur
		cur.next = &newNode
		cur.next.prev = &newNode
	}
	d.len++
	return nil
}

func (d *DoubleLinkedList) InsertAtEnd(c Commit) {
	tmp := Node{data: &c}
	if d.head == nil {
		d.head = &tmp
	} else {
		p := d.head
		for ; p.next != nil; p = p.next {
		}
		p.next = &tmp
		tmp.prev = p
	}
	d.len++
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n > d.len {
		return fmt.Errorf("Length is less than %d", n)
	}
	cur := d.head
	if n == 1 {
		d.head = d.head.next
		d.head.prev = nil
	} else {
		for i := 0; i < n; i++ {
			cur = cur.next
		}
		cur.prev.next = cur.next
		cur.next.prev = cur.prev
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("Don't have current")
	}
	if d.curr == d.head {
		d.head.next = nil
		d.head = d.curr
		d.head.prev = nil

	} else if d.curr == d.tail {
		d.tail.prev = nil
		d.tail = d.curr
		d.tail.next = nil
	} else {
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
	}
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return 0, fmt.Errorf("No such index")
	}
	i := 1
	cur := d.head
	for cur != d.curr {
		cur = cur.next
		i++
	}
	return i, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.head == nil {
		return nil
	}
	head := d.head
	d.head.next.prev = nil
	d.head = d.head.next
	return head
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.tail == nil {
		return nil
	}
	tail := d.tail
	d.tail.prev.next = nil
	d.tail = d.tail.prev
	return tail
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	head := d.head
	for ; head != nil; head = head.next {
		if head.data.UUID == uuID {
			return head
		}
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	head := d.head
	for ; head != nil; head = head.next {
		if head.data.Message == message {
			return head
		}
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	curr := d.head
	var nextNode *Node
	d.head, d.tail = d.tail, d.head
	for curr != nil {
		nextNode = curr.next
		curr.prev, curr.next = curr.next, curr.prev
		curr = nextNode
	}
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

// func GenerateJSON() {
// 	// Дополнительное задание написать генератор данных
// 	// используя библиотеку gofakeit
// }

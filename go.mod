module gitlab.com/mezigar/go-kata-course-2

go 1.19

require gitlab.com/mezigar/greet v0.0.0-20230213130303-13b9c4f09d80

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/rs/zerolog v1.29.1
	github.com/tebeka/selenium v0.9.9
	golang.org/x/sync v0.1.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/render v1.0.2
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
